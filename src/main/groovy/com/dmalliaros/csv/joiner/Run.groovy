package com.dmalliaros.csv.joiner

import groovy.io.FileType
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.StringUtils

/**
 * The csv.joiner program implements an application that
 * merge columns from 2 csv files in side to file system.
 *
 * @author  dmalliaros
 * @version 1.0
 * @since   2016-08-22
 */
class Run {

    public static void main(String[] args) {

        def dir = new File("/tmp")
        dir.eachFileRecurse(FileType.FILES) { fileItem ->
            def data = fileItem =~ /\w+PRICE_(\d+)/
            if (!data) {
                return
            }

            Map<Key, Object> result = new HashMap<>()

            def code = data.getAt(0).getAt(1)

            Reader barcodeFile = new InputStreamReader(
                    new FileInputStream("/tmp/ERP_PRODUCT_${code}.csv"),
                    "windows-1253")

            Reader receiptsFile = new InputStreamReader(
                    new FileInputStream("/tmp/ERP_PRODUCT_PRICE_${code}.csv"),
                    "windows-1253")

            def format = CSVFormat.newFormat(';' as char)
            Iterable<CSVRecord> barcodes = format.parse(barcodeFile);
            for (CSVRecord record : barcodes) {


                def _k = new Key(
                        record.get(0),
                        record.get(1),
                        StringUtils.leftPad(record.get(2), 3, "0")

                );

                println _k
                if (!result.containsKey(_k)) {

                    result.put(
                            _k
                            ,
                            [
                                    record.get(0),  // product code
                                    record.get(1),  // color
                                    record.get(2),  // size
                                    record.get(3),  // meta-data
                                    "",     // price
                            ])

                }
            }


            Iterable<CSVRecord> receipts = format.parse(receiptsFile);
            for (CSVRecord record : receipts) {


                def _k = new Key(
                        record.get(0), // product code
                        record.get(1), // color
                        StringUtils.leftPad(record.get(2), 3, "0") // size
                );


                if (!result.containsKey(_k)) {
                    println "ERROR =>> ${_k} ==>> ${record}"
                    continue
                }

                result[_k][4] = record.get(3) // price
            }

            def file = new File("/tmp/PRICE_${code}.csv")

            if (file.exists()) {
                file.delete()
            }


            file.createNewFile()


            result.each { k, v ->

                FileUtils.writeLines(file, "UTF-8", [StringUtils.join(v, ";")], true)

            }


        }


    }


    static class Key{
        String code
        String color
        String size

        Key(String code, String color, String size) {
            this.code = code
            this.color = color
            this.size = size
        }

        boolean equals(o) {
            if (this.is(o)) return true
            if (getClass() != o.class) return false

            Key key = (Key) o

            if (!code.equals(key.code)) return false
            if (!color.equals(key.color)) return false
            if (!size.equals(key.size)) return false

            return true
        }

        int hashCode() {
            int result
            result = code.hashCode()
            result = 31 * result + color.hashCode()
            result = 31 * result + size.hashCode()
            return result
        }


        @Override
        public String toString() {
            return "Key{" +
                    "code='$code'" +
                    ", color='$color'" +
                    ", size='$size'" +
                    '}';
        }
    }

}

